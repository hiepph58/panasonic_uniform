<html lang="vi">

<head>
  <title>Uniform</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<body>

  <div class="col-md-6 col-md-push-3">
    <h1>Đăng Ký Đồng Phục</h1>
    <div class="row">
      <form id="form_dk" class="" action="" method="post">
        <div class="col-sm-12 nopadding">
          <div class="form-group">
            <input type="text" class="form-control" id="id" name="id" value="" placeholder="MNV">
          </div>
        </div>
        <div class="col-sm-12 nopadding">
          <div class="form-group">
            <input type="text" class="form-control" id="name" name="name" value="" placeholder="Họ và tên">
          </div>
        </div>
        <div class="col-sm-12 nopadding">
          <div class="form-group">
            <input type="text" class="form-control" id="email" name="email" value="" placeholder="Email">
          </div>
        </div>
        <div class="col-sm-12 nopadding">
          <div class="form-group">
            <input type="text" class="form-control" id="phone" name="phone" value="" placeholder="Số điện thoại">
          </div>
        </div>

        <div class="col-sm-12 nopadding">
          <div class="form-group">
            <input type="text" class="form-control" id="dept" name="dept" value="" placeholder="Bộ phận">
          </div>
        </div>


        <div class="col-sm-12 nopadding">
          <div class="form-group">
            <input type="text" class="form-control" id="gen" name="gen" value="" placeholder="Giới tính">
          </div>
        </div>

        <div class="col-sm-12 nopadding">
          <div class="form-group">
            <input type="text" class="form-control" id="history" name="history" value="" placeholder="Lịch Sử">
          </div>
        </div>
        <div class="col-sm-12 nopadding">
          <div class="form-group">
            <div class="input-group">
              <div class="input-group-btn">
                <select class="form-control" id="educationDate" name="educationDate">
                  <option value="">Loại yêu cầu</option>
                  <option value="2015">Định kỳ</option>
                  <option value="2016">Đặc biệt</option>
                </select>

                <button class="btn btn-success" type="button" onclick="education_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button>
              </div>
            </div>
          </div>

        </div>
        <div class="col-sm-12 nopadding">
          <input type="submit" name="" class="btn btn-info" value="Submit">

        </div>
      </form>
      <div id ="showData"></div>
    </div>

  </div>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#form_dk").submit(function(e) {
        e.preventDefault();
       
        $.ajax({
          url: "config.php",
          type: "POST",
          dataType: 'json',
          data: $("#form_dk").serialize(),
          success: function(data) {
            //alert(JSON.stringify(data));
            var myJsonData = JSON.stringify(data);
            console.log("Success"+ myJsonData); 
            
            $.each(data, function(index, element) {
              $("#name").val(element.name);
              $("#email").val(element.email);
              $("#phone").val(element.phone);
              $("#dept").val(element.dept);
              $("#gen").val(element.gen);
              $("#history").val(element.history);
              $("#educationDate").val(element.educationDate);
              //console.log (element.dept);
            });
            
           
          },

          error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
          }
        });
      });
    });
  </script>
  <script>
    function addDataJson(params) {
      
      
    }
  </script>
  <script>
    var room = 1;

    function education_fields() {

      room++;
      var objTo = document.getElementById('education_fields')
      var divtest = document.createElement("div");
      divtest.setAttribute("class", "form-group removeclass" + room);
      var rdiv = 'removeclass' + room;
      divtest.innerHTML = '<div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="id" name="id" value="" placeholder="MNV"></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="name" name="name" value="" placeholder="Họ và tên"></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="dept" name="dept" value="" placeholder="Bộ phận"></div></div><div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <select class="form-control" id="educationDate" name="educationDate"><option value="">Date</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option> </select><div class="input-group-btn"> <button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div><div class="clear"></div>';

      objTo.appendChild(divtest)
    }

    function remove_education_fields(rid) {
      $('.removeclass' + rid).remove();
    }
  </script>

</body>

</html>